import requests
import threading

from django.conf import settings

from .utils import jsonify, if_language_exists, generate_buttons, nickname_or_title


class SurveyExtractor(object):

    def __init__(self):
        self.survey_headers = {
                               "Authorization":"Bearer {}".format(settings.ACCESS_TOKEN),
                               "Content-Type":"application/json"
                              }

    def request_data(self, url):
        self.survey_request = requests.session()
        self.survey_request.headers.update(self.survey_headers)
        return self.survey_request.get(url)

    def retrieve_folders(self, page=1, per_page=10):
        folders_url = 'https://api.surveymonkey.com/v3/survey_folders?page={}&per_page={}'.format(page, per_page)
        self.api_response = self.request_data(folders_url)
        return jsonify(self.api_response)

    def retrieve_folder(self, folder_id, page=1, per_page=10):
        folder_url = 'https://api.surveymonkey.com/v3/surveys?page={}&per_page={}&folder_id={}'.format(page, per_page, folder_id)
        self.api_response = self.request_data(folder_url)
        survey_array = []

        def generate_survey_data(api_data):
            survey_data = [nickname_or_title(api_data), if_language_exists(api_data['id']), api_data['id'], generate_buttons(self.retrieve_survey(api_data['id']))]
            survey_array.append(survey_data)
            return survey_data

        threads = {}
        for index, api_data in enumerate(jsonify(self.api_response)['data']):
            threads[index] = threading.Thread(target=generate_survey_data, args=(api_data,))
            threads[index].start()

        for index in threads:
            threads[index].join()

        return survey_array

    def retrieve_survey_details(self, survey_id):
        details_url = 'https://api.surveymonkey.com/v3/surveys/{}/details'.format(survey_id)
        self.api_response = self.request_data(details_url)
        return jsonify(self.api_response)

    def retrieve_survey(self, survey_id):
        from .models import Urls
        try:
            urls = Urls.objects.get(pk=survey_id)
            return {'analyze_url': urls.analyze_url, 'edit_url': urls.edit_url, 'preview': urls.preview, 'summary_url': urls.summary_url}
        except:
            survey_url = 'https://api.surveymonkey.com/v3/surveys/{}'.format(survey_id)
            self.api_response = self.request_data(survey_url)
            survey_dict = jsonify(self.api_response)
            new_url = Urls.objects.create(pk=survey_id, analyze_url=survey_dict['analyze_url'], edit_url=survey_dict['edit_url'], preview=survey_dict['preview'], summary_url=survey_dict['summary_url'])
            new_url.save()
            return survey_dict
