import os, pdb
import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import default_storage

from .models import SurveyFolder, Survey
from .survey_class import SurveyExtractor
from .process_surveys import get_all_details, get_all_responses

def get_folders(request):
    se = SurveyExtractor()
    survey_folders = json.dumps(se.retrieve_folders(per_page=9999))
    return HttpResponse(survey_folders, content_type='application/json')

def get_folder_by_id(request, folder_id):
    se = SurveyExtractor()
    survey_folder = json.dumps(se.retrieve_folder(folder_id=folder_id, per_page=9999))
    return HttpResponse(survey_folder, content_type='application/json')

@csrf_exempt
def get_survey_details(request):
    se = SurveyExtractor()
    if request.method=='POST' and request.is_ajax():
        survey_ids = request.POST.getlist('surveyIds[]')
        survey_details = json.dumps(se.retrieve_survey_details(survey_id=survey_ids[0]))
        all_survey_details = get_all_details(survey_details)
        all_responses_data = get_all_responses(all_survey_details)
        return HttpResponse(survey_details, content_type='application/json')

@csrf_exempt
def save_survey_language(request, survey_id):
    if request.method == 'POST' and request.is_ajax():
        input_language = request.POST['input_language']
        all_surveys = Survey.objects.all()
        current_survey = all_surveys.filter(survey_id=survey_id)
        if current_survey:
            survey_to_update = Survey.objects.get(pk=survey_id)
            if survey_to_update.localized_language != input_language:
                survey_to_update.localized_language = input_language
                survey_to_update.save()
                return JsonResponse({'status':'Success', 'msg': 'updated successfully'})
            else:
                return JsonResponse({'status':'Success', 'msg': 'nothing to update'})
        else:
            survey_to_update = all_surveys.create(pk=survey_id, localized_language=input_language)
            survey_to_update.save()
            return JsonResponse({'status':'Success', 'msg': 'created successfully'})

@csrf_exempt
def upload_qa_file(request):
    if request.method=='POST':
        survey_folders = SurveyFolder.objects.all()
        uploaded_file = request.FILES['file']
        uploaded_filename = request.POST['folder_name']
        try:
            survey_folder = survey_folders.get(pk=request.POST['folder_id'])
            survey_folder.folder_name = uploaded_filename
            survey_folder.qa_file = uploaded_file
            survey_folder.save()
            return JsonResponse({'status':'Success', 'msg': 'updated successfully'})
        except:
            survey_folder = SurveyFolder.objects.create(folder_id=request.POST['folder_id'], folder_name=uploaded_filename, qa_file=uploaded_file)
            survey_folder.save()
            return JsonResponse({'status':'Success', 'msg': 'created successfully'})
