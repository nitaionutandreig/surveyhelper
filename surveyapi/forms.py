from django import forms

from .models import SurveyFolder


class SurveyFolderForm(forms.Form):
    folder_id = forms.CharField()
    folder_name = forms.CharField()
    qa_file = forms.FileField()
