import json

def jsonify(api_response):
    ''' jsonify function for surveymonkey api response '''
    return json.loads(api_response.content.decode('utf-8'))

def nickname_or_title(response):
    ''' Check if survey has nickname, if not return title '''
    return response['nickname'] if response['nickname'] else response['title']

def generate_survey_form(language=''):
    ''' Return html form for the localized language survey input '''
    if language:
        return '''<form method="post">
                    <div class="input-group">
                      <input type="text" class="form-control saved" id="localized-language" value="{}">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-success" id="save-survey-input">
                            <span class="glyphicon glyphicon-ok"></span>
                          </button>
                          <button type="reset" class="btn btn-danger" id="reset-survey-input">
                          	<span class="glyphicon glyphicon-remove"></span>
                          </button>
                        </span>
                     </div>
                  </form>'''.format(language)
    else:
        return '''<form method="post">
                    <div class="input-group">
                      <input type="text" class="form-control not-saved" id="localized-language" placeholder="Enter a language">
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-success" id="save-survey-input">
                            <span class="glyphicon glyphicon-ok"></span>
                          </button>
                          <button type="reset" class="btn btn-danger" id="reset-survey-input">
                          	<span class="glyphicon glyphicon-remove"></span>
                          </button>
                        </span>
                     </div>
                  </form>'''

def if_language_exists(survey_id):
    '''
    Checks if a survey ID exists in the database and returns the localized language.
    If no survey ID, returns a html input tag
    '''
    from .models import Survey
    current_survey = Survey.objects.filter(survey_id=survey_id)
    if current_survey:
        try:
            return generate_survey_form(current_survey.values_list('localized_language', flat=True).get(pk=survey_id))
        except:
            return generate_survey_form()
    else:
        return generate_survey_form()

def generate_buttons(survey_response):
    ''' Function that dinamically generates the buttons for each survey '''
    return '''<div style="display: flex;">
                <div class="btn btn-primary" style="margin-right: 5px; flex: 1"><a href="{}" target="_blank" style="text-decoration: none; color: white;">Analyze</a></div>
                <div class="btn btn-primary" style="margin-right: 5px; flex: 1"><a href="{}" target="_blank" style="text-decoration: none; color: white;">Edit</a></div>
                <div class="btn btn-primary" style="margin-right: 5px; flex: 1"><a href="{}" target="_blank" style="text-decoration: none; color: white;">Preview</a></div>
                <div class="btn btn-primary" style="margin-right: 5px; flex: 1"><a href="{}" target="_blank" style="text-decoration: none; color: white;">Summary</a></div>
              </div>
           '''.format(survey_response["analyze_url"], survey_response["edit_url"], survey_response["preview"], survey_response["summary_url"])
