# Generated by Django 2.0.1 on 2018-02-28 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('survey_id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('localized_language', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('last_modified_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['survey_id'],
            },
        ),
        migrations.CreateModel(
            name='SurveyFolder',
            fields=[
                ('folder_id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('folder_name', models.CharField(max_length=50)),
                ('qa_file', models.FileField(upload_to='qa_files/')),
                ('uploaded_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['folder_id'],
            },
        ),
        migrations.CreateModel(
            name='Urls',
            fields=[
                ('survey_id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('analyze_url', models.CharField(max_length=200)),
                ('edit_url', models.CharField(max_length=200)),
                ('preview', models.CharField(max_length=200)),
                ('summary_url', models.CharField(max_length=200)),
            ],
            options={
                'ordering': ['survey_id'],
            },
        ),
    ]
