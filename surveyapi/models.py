from django.db import models


class SurveyFolder(models.Model):
    folder_id = models.CharField(max_length=50, primary_key=True)
    folder_name = models.CharField(max_length=50)
    qa_file = models.FileField(upload_to='qa_files/')
    uploaded_at = models.DateTimeField(auto_now=True)

    # Metadata
    class Meta:
        ordering = ["folder_id"]

    # Return folder name
    def __str__(self):
        return self.folder_name


class Survey(models.Model):
    survey_id = models.CharField(max_length=50, primary_key=True)
    localized_language = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    last_modified_at = models.DateTimeField(auto_now=True)

    # Metadata
    class Meta:
        ordering = ["survey_id"]

    # Return localized language
    def __str__(self):
        return self.localized_language


class Urls(models.Model):
    survey_id = models.CharField(max_length=50, primary_key=True)
    analyze_url = models.CharField(max_length=200)
    edit_url = models.CharField(max_length=200)
    preview = models.CharField(max_length=200)
    summary_url = models.CharField(max_length=200)

    # Metadata
    class Meta:
        ordering = ["survey_id"]

    # Return localized language
    def __str__(self):
        return self.survey_id
