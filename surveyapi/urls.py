from django.urls import path

from . import views

urlpatterns = [
    path('get_folders/', views.get_folders, name='get_folders'),
    path('get_folder_by_id/<int:folder_id>/', views.get_folder_by_id, name='get_folder_by_id'),
    path('get_survey_details/', views.get_survey_details, name='get_survey_details'),
    path('save_survey_language/<int:survey_id>/', views.save_survey_language, name='save_survey_language'),
    path('upload_qa_file/', views.upload_qa_file, name='upload_qa_file'),
]
