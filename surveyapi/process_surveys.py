import json
import pandas as pd

def get_all_details(content):
    content_dict = json.loads(content)
    df_questions = pd.DataFrame(columns=['id', 'text'])
    df_answers = pd.DataFrame(columns=['question_id', 'id', 'text'])

    for page in content_dict['pages']:
        for question in page['questions']:
            df_questions.loc[len(df_questions)] = [question['id'], question['headings'][0]['heading'].replace('<br>', '')]
            if 'answers' in question:
                for answer in question['answers']['choices']:
                    df_answers.loc[len(df_answers)] = [question['id'], answer['id'], answer['text']]

    return df_questions, df_answers

def get_all_responses(survey_details):
    df_questions = survey_details[0]
    df_answers = survey_details[1]
