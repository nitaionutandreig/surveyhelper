from django.conf import settings


def global_settings(request):
    # return any necessary values
    return {
        'BICENTRAL_DATA': settings.BICENTRAL_DATA,
    }
