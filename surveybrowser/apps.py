from django.apps import AppConfig


class SurveybrowserConfig(AppConfig):
    name = 'surveybrowser'
