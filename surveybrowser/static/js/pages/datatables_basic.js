// Datatable custom JS file

// notify function based on jGrowl
function notify(message, title, style) {
  $.jGrowl(message, {
    header: title,
    theme: 'alert-styled-left bg-' + style,
    speed: 150,
  });
}

// set block config variable for block ui
var block_config = {
    message: '<div class="custom-spinner sp-dark"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>',
    overlayCSS: {
     backgroundColor: '#fff',
     opacity: 0.8,
     cursor: 'wait'
    },
    css: {
     border: 0,
     padding: 0,
     backgroundColor: 'none'
   }
  };

$(function() {
    // Survey Folder Table initialization
    // -----------------------------------------------------------------------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    // Survey folder table
    // -----------------------------------------------------------------------------------------------

    var surveyFoldersTable = $('.datatable-basic').DataTable({
        fixedHeader: true
    });

    // Highlighting rows and columns on mouseover
    $('.datatable-basic tbody').on('mouseover', 'td', function() {
        var lastIdx = null;
        var colIdx = surveyFoldersTable.cell(this).index().column;

        if (colIdx !== lastIdx) {
            $(surveyFoldersTable.cells().nodes()).removeClass('active');
            $(surveyFoldersTable.column(colIdx).nodes()).addClass('active');
        }
    }).on('mouseleave', function() {
        $(surveyFoldersTable.cells().nodes()).removeClass('active');
    });

    // Single row selection
    var singleSelect = $('.datatable-selection-single').DataTable();
    $('.datatable-selection-single tbody').on('click', 'tr', function() {
      if ($(this).hasClass('success')) {
      } else {
        singleSelect.$('tr.success').removeClass('success');
        $(this).addClass('success');
        var nbSurveys = $(this).find("td:eq(1)").text();
        var surveyId = $(this).find("td:eq(2)").text();
        if (nbSurveys != 0) {
          $('#survey-content-table').block(block_config);
          $.get('get_folder_by_id/' + surveyId + '/').done(function(data) {
             surveyContentTable.fnClearTable();
             $('#survey-content-table').unblock();
             surveyContentTable.fnAddData(data)
          });
        }
      }
    });

    // Folder content table
    // -----------------------------------------------------------------------------------------------

    // AJAX sourced data for survey table
    var surveyContentTable = $('.datatable-ajax').dataTable({
      "columnDefs": [{
                      "targets": [0, 3],
                      "orderable": false,
                    }],
      dom: '<"dt-buttons-full"B><"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
      buttons: [
          {extend: 'selectAll', className: 'btn bg-blue'},
          {extend: 'selectNone', className: 'btn bg-blue'},
      ],
      select: true,
    });

    // Add cursor pointer to survey content table row
    $('#survey-content-table tbody').on('mouseover', 'tr', function(event) {
        $(this).css('cursor', 'pointer')
    });

    // Select survey only if localized languange is filled
    $('#survey-content-table tbody').on('click', 'tr', function(event) {
      if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
      }
      else {
        var localizedLanguage = $(this).find("#localized-language").val()
        if (localizedLanguage) {
          $(this).addClass('selected');
        } else {
          notify('Please input a localized language before selecting a survey.', 'Info', 'info')
        }
      }
    });

    // Add class modified if editing input and disabling enter key
    $('#survey-content-table tbody').on('keyup keypress', '#localized-language', function(event) {
      $(this).addClass('modified')
      if (event.keyCode === 13) {
        event.preventDefault()
        return false;
      }
    });

    // Disable row select if clicking on anchor, input or button tags
    tags = ['a', 'input', 'button']
    for (index in tags) {
      $('#survey-content-table tbody').on('click', tags[index], function(event) {
        event.stopPropagation();
      });
    }

    // Save localized language to database when clicking button
    $('#survey-content-table tbody').on('click', '#save-survey-input', function(event) {
      var surveyId = $(this).closest('tr').find("td:eq(2)").text();
      $(this).closest('tr').find("input").each(function() {
        if (this.value) {
          if ($(this).hasClass('not-saved') || $(this).hasClass('modified')) {
            $.post('save_survey_language/' + surveyId + '/',
                   {input_language: this.value}
                  );
            $(this).addClass('saved').removeClass('not-saved').removeClass('modified');
            notify('Localized Language Saved.', 'Success', 'success')
          } else {
            notify('This localized language is already saved.', 'Info', 'info')
          }
        } else {
          notify('Please add a language first before saving.', 'Attention', 'danger')
        }
      });
    });

    // Generic table functionality
    // -----------------------------------------------------------------------------------------------

    // Alternative pagination
    $('.datatable-pagination').DataTable({
        pagingType: "simple_numbers",
        language: {
            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
        }
    });

    // Modal template
    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';

    // Buttons inside zoom modal
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };

    // Icons inside zoom modal classes
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };

    // Q&A File upload initialization
    function initFileInput(folder_id, folder_name) {
      $(".fileupload").fileinput({
          uploadUrl: "http://localhost:8080/upload_qa_file/", // server upload action
          maxFileCount: 1,
          uploadExtraData: {'folder_id': folder_id, 'folder_name': folder_name},
          initialCaption: "No Q&A file uploaded.",
          initialPreview: [],
          fileActionSettings: {
              removeIcon: '<i class="icon-bin"></i>',
              removeClass: 'btn btn-link btn-xs btn-icon',
              uploadIcon: '<i class="icon-upload"></i>',
              uploadClass: 'btn btn-link btn-xs btn-icon',
              zoomIcon: '<i class="icon-zoomin3"></i>',
              zoomClass: 'btn btn-link btn-xs btn-icon',
              indicatorNew: '<i class="icon-file-plus text-slate"></i>',
              indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
              indicatorError: '<i class="icon-cross2 text-danger"></i>',
              indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>',
          },
          layoutTemplates: {
              icon: '<i class="icon-file-check"></i>',
              modal: modalTemplate
          },
          initialCaption: "No file selected",
          previewZoomButtonClasses: previewZoomButtonClasses,
          previewZoomButtonIcons: previewZoomButtonIcons
      });
    }

    // Add QA file to a survey folder
    $('.datatable-selection-single tbody').on("click", "#add-qa-file", function (event) {
      var folderId = $(this).data('folder-id');
      var folderName = $(this).data('folder-name');
      $(".modal-body").attr('id', folderId);
      $(".modal-body").attr('name', folderName);
      initFileInput(folderId, folderName)
    });

    // Close modal, clear file input and notify user the file was uploaded
    $('.fileupload').on('fileuploaded', function(event, data, previewId, index) {
      $('.fileupload').fileinput('destroy');
      $('#close-modal').click()
      $('.success').find('#qa-filename').html('File uploaded.')
      notify('The file was successfully uploaded.', 'Success', 'success')
    });

    // Process Surveys Button Interaction
    // -----------------------------------------------------------------------------------------------
    $('#process-survey-button').on('click', function() {
      selectedRows = $(surveyContentTable.fnGetNodes()).filter('.selected');
      if (selectedRows.length > 0) {
        var allLanguagesSaved = true
        $(selectedRows).each(function() {
          if ($(this).closest('tr').find("#localized-language").val() == "") {
            allLanguagesSaved = false;
            notify('Please add a localized language to each of the surveys before processing.', 'Attention', 'danger')
            return false;
          }
        });
        if (allLanguagesSaved) {
          console.log('Processing Survey')
          var surveyIds = []
          $(selectedRows).each(function() {
              surveyIds.push($(this).find("td:eq(2)").text())
          });
          $('.page-container').block(block_config);
          $.post('get_survey_details/', {'surveyIds[]': surveyIds}).done(function(data) {
                 console.log(data)
                 $('.page-container').unblock()
               });
        }
      } else {
        notify('Please select surveys to process.', 'Attention', 'danger')
      };
    });

    // External table additions
    // -----------------------------------------------------------------------------------------------

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
      minimumResultsForSearch: Infinity,
      width: 'auto'
    });
});
