from django.contrib import admin
from surveyapi.models import Survey, SurveyFolder


class SurveyFolderAdmin(admin.ModelAdmin):
    list_display = ["folder_id", "folder_name", "qa_file", "uploaded_at"]

    # link model Admin to model
    class Meta:
        model = SurveyFolder


class SurveyAdmin(admin.ModelAdmin):
    list_display = ["survey_id", "localized_language", "created_at", "last_modified_at"]

    # link model Admin to model
    class Meta:
        model = Survey


# Register your models here.
admin.site.register(Survey, SurveyAdmin)
admin.site.register(SurveyFolder, SurveyFolderAdmin)
