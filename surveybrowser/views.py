import json

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages

from django.http import HttpResponse
from surveyapi.views import get_folders, get_folder_by_id, get_survey_details
from .utils import if_file_exists

def login_user(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            if user is not None:
                login(request, user)
                return redirect('/')
        else:
            messages.error(request, 'Invalid Username or Password.')
    else:
        form = AuthenticationForm()

    return render(request, 'registration/login.html', {'form': form})

@login_required(login_url='login/')
def index(request):
    folders_data = json.loads(get_folders(request).content)
    folders_data = if_file_exists(folders_data['data'])
    return render(request, 'index.html', {'folders_data': folders_data})

def process_survey(request, survey_id):
    response_data = get_survey_details(survey_id)
    return render(request, 'response_data.html', {'response_data': response_data})

def logout_user(request):
    """
    Log users out and re-direct them to the main page.
    """
    logout(request)
    return redirect('/')
