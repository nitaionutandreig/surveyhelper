'''
Utility file for survey browser views
'''

def if_file_exists(folders_data):
    from surveyapi.models import SurveyFolder
    survey_folders = SurveyFolder.objects.all()
    for folder in folders_data:
        try:
            qa_filename = survey_folders.get(pk=folder['id']).qa_file.name.split('/')[1]
            folder['qa_file'] = qa_filename
        except:
            folder['qa_file'] = 'No QA file uploaded.'
    return folders_data
